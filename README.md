# Not A Virus minigame

<img src="/uploads/299a0c69025af1fea480b7b0ee2e306b/bandicam_2021-11-20_17-53-35-377.jpg"  width="400" >
<img src="/uploads/4f72b4c457017cdbe205a8bdcbb5aab6/bandicam_2021-11-20_17-56-23-282.jpg"  width="400" >
<img src="/uploads/55ea1643f2d1947061b904d3c3eff587/bandicam_2021-11-20_17-57-10-471.jpg"  width="400" >
<img src="/uploads/addfdbab85deb2819cba0d1435df84f2/bandicam_2021-11-20_17-58-42-587.jpg"  width="400" >
<img src="/uploads/07c43cbbf544a5283b9838cf30944174/bandicam_2021-11-20_17-59-33-074.jpg"  width="400" >
<img src="/uploads/71e36ab954748a0ee4e9232003d48253/bandicam_2021-11-20_17-59-53-085.jpg"  width="400" >

**The story overview:** <br>
Not a Virus is a point and click game, where you can meet a strange entity while using your computer. During your playtime, you will get to know it more and how it got there.

**Controls:** <br>
The only things you need is a mouse and a keyboard 🙂
Pretty straightforward!

**How to access the game:** <br>
You can choose from a downloadable Windows, Linux builds.

**Notes from the developers:** <br>
The game was made during a two week Game Jam by two people. We have many more ideas how to improve upon the story and gameplay, unfortunately due to time constraints we couldn’t do it. 
It’s possible we will do it later! 🙂

We had fun creating the game, we hope you will like it too 🙂

🎮 ITCH.io: https://petrahugyecz.itch.io/not-a-virus  <br>
:eyes:  Youtube teaser: https://www.youtube.com/watch?v=cIk11UxzGhY
